/* *
 * CAROUSEL
 * */
var Carousel = React.createClass({
  render: function () {
    var settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      className:"opera-carousel",
      adaptiveHeight: true,
      centerMode: true,
      lazyLoad:false,
      swipe:true,
      swipeToSlide:true,
      dotsClass:"opera-carousel__dots slick-dots",
      autoplay:false
      // afterChange () {
      //   var activeSlide = document.getElementsByClassName("slick-active slick-slide");
      //   console.log(activeSlide);
      //   var  activeDataIndex = activeSlide[0].getAttribute('data-index');
      //   console.log(activeDataIndex);
      // }
    };
    return (
        <Slider {...settings}>
          <div className="carousel-placeholder">
            <div id="carousel-01" className="carousel-single">
              <div className="opera-carousel__slide-info">
                <h3 className="text-center">Campaign Name</h3>
                <p className="text-center desc">Lorem ipsum dolor sit amet. Neque porro quisquam est qui dolorem ipsum quia dolor.</p>
                <p className="text-center"><button className="btn-border btn-launch-add">Launch Ad</button></p>
              </div>
            </div>
          </div>
          <div className="carousel-placeholder">
            <div id="carousel-02" className="carousel-single">
              <div className="opera-carousel__slide-info">
                <h3 className="text-center">Campaign Name</h3>
                <p className="text-center">Lorem ipsum dolor sit amet. Neque porro.</p>
                <p className="text-center"><button className="btn-border btn-launch-add">Launch Ad</button></p>
              </div>
            </div>
          </div>
          <div className="carousel-placeholder">
            <div id="carousel-03" className="carousel-single">
              <div className="opera-carousel__slide-info">
                <h3 className="text-center">Campaign Name</h3>
                <p className="text-center">Lorem ipsum dolor sit amet. Neque porro quisquam est qui dolorem ipsum quia dolor.</p>
                <p className="text-center"><button className="btn-border btn-launch-add">Launch Ad</button></p>
              </div>
            </div>
          </div>
          <div className="carousel-placeholder">
            <div id="carousel-04" className="carousel-single">
              <div className="opera-carousel__slide-info">
                <h3 className="text-center">Campaign Name</h3>
                <p className="text-center">Lorem uisquam est qui dolorem ipsum quia dolor</p>
                <p className="text-center"><button className="btn-border btn-launch-add">Launch Ad</button></p>
              </div>
            </div>
          </div>
        </Slider>
    );
  }
});

var App = React.createClass({

  getInitialState: function() {
    return {
      data: []
    }
  },

  componentDidMount: function() {
    this.serverRequest = $.get(this.props.webApiUrl, function (result) {
        var singleCarouselData = result;
        console.log(singleCarouselData);
        this.setState({
            name: singleCarouselData.name,
            date: singleCarouselData.date,
            agency: singleCarouselData.agency,
            tagsV: singleCarouselData.verticals,
            description: singleCarouselData.description
        });
    }.bind(this));
  },

  componentWillUnmount: function() {
    this.serverRequest.abort();
  },

  render: function() {

    return (
        <div>
          {this.state.data.map(function() {
            return (
                <div key={this.state.name} className="carousel-placeholder">
                  <div id="carousel-01" className="carousel-single">
                    <div className="opera-carousel__slide-info">
                      <h3 className="text-center">{this.state.name}</h3>
                      <p className="text-center desc">Lorem ipsum dolor sit amet. Neque porro quisquam est qui dolorem ipsum quia dolor.</p>
                      <p className="text-center"><button className="btn-border btn-launch-add">Launch Ad</button></p>
                    </div>
                  </div>
                </div>
            );
          })}
        </div>
    )
  }
});



ReactDOM.render(
    <App webApiUrl="http://localhost:63342/opera-apollo/campaigns.json" />,
    // <Carousel/>
    document.getElementById('content')
);


