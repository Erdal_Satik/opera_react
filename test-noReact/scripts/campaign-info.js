var CampaignInfo = React.createClass({
  render: function() {
    return (
        <div className="campaign-detail">
          <img className="campaign-image" src='http://fakeimg.pl/400x400/f0f0f0/999/?text=01' height="133px" width="133px"/>
          <ul>
            <li>
              <span className="label">CAMPAIGN</span>
              <span className="campaig-name">ADIDAS-PONETE LA VERDE</span>
            </li>
            <li>
              <span className="label">LIVE DATE</span>
              01/12/2016
            </li>
            <li>
              <span className="label">AGENCY NAME</span>
              Loremcy
            </li>
            <li>
              <span className="label">DESCRIPTION</span>
              Lorem ipsum dolor sit amet. Neque porro quisquam est qui dolorem ipsum quia dolor..
            </li>
          </ul>
          <p className="text-center"><button className="btn-border btn-launch-add">Launch Ad</button></p>
        </div>
    );
  }
});


ReactDOM.render(
    //<CommentBox url="/api/comments" pollInterval={2000} />,
    <CampaignInfo/>,
    //<CarouselSlide/>,
    document.getElementById('content')
);
