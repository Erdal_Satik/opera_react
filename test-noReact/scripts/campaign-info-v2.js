var App = React.createClass({
    getInitialState: function () {
        // return { data: []};
        return {
            name: "",
            date: "",
            agency: "",
            verticals: "",
            description: ""
        };
    },

    componentDidMount: function () {
        console.log("componentDidMount yes");

        // Jockey.on("campaignInfo", function (payload) {
        //
        //     if (typeof payload.campaignInfo == "undefined"){
        //         console.log("Empty");
        //         return;
        //     }
        //
        //     console.log("campaignInfo Event Recevied");
        //     console.log(payload);
        //
        //     var singleCarouselData = payload;
        //
        //     this.setState({
        //         name: singleCarouselData.name,
        //         date: singleCarouselData.date,
        //         agency: singleCarouselData.agency,
        //         verticals: singleCarouselData.verticals,
        //         description: singleCarouselData.description
        //     });
        //
        //     console.log(this.setState());
        //
        // }.bind(this));

        setTimeout(function(){
            var launchButton = document.getElementById("launch-btn"),
                launchLoadAnim = document.getElementById("launch-load-anim");

            launchButton.addEventListener("click", function (e) {
                launchButton.classList = "btn-border btn-launch-add-v2 disabled";

                Jockey.send("onClick");
                console.log("onClick Event Send")

            }, false);

            Jockey.on("adPrepared", function (payload) {
                console.log("adPrepared Event Received");
                console.log(payload);

                launchButton.classList = "btn-border btn-launch-add-v2";

            });

            Jockey.send("campaignInfo");
            console.log("campaignInfo Event Send To Server")

        }, 100);


    },
    componentWillUnmount: function () {
        //this.serverRequest.abort();
    },
    render: function () {
        return (
            <div key={this.state.name} className="campaign-detail-v2">
                <div className="campaign-image-v2">
                    <p id="action-btn-holder" className="text-center">
                        <a href="#" id="launch-btn" className="btn-border btn-launch-add-v2">
                            <span className="launch-btn-text">Launch Ad</span>
                            <div id="launch-load-anim" className="spinner">
                                <div className="double-bounce1"></div>
                                <div className="double-bounce2"></div>
                            </div>
                        </a>
                    </p>
                </div>
                <ul className="campaign-info-v2">
                    <li>
                        <span className="label-v2">LIVE DATE</span>
                        <span className="info-text pull-right">{this.state.date}</span>
                    </li>
                    <li>
                        <span className="label-v2">AGENCY </span>
                        <span className="info-text pull-right">{this.state.agency}</span>
                    </li>
                    <li>
                        <span className="label-v2 display-block">VERTICALS</span>
                        <span className="info-text">{this.state.verticals}</span>
                    </li>
                    <li>
                        <span className="label-v2 display-block">DESCRIPTION</span>
                        <span className="info-text">{this.state.description}</span>
                    </li>
                </ul>
            </div>
        )
    }
});

ReactDOM.render(
    <App />,
    document.getElementById('content')
);
